# Data Gathering from Walker 2009 

Some sequences and metadata from:

Laura M. Walker et al. 
Broad and Potent Neutralizing Antibodies from an African Donor Reveal a New HIV-1 Vaccine Target.
Science Volume 326, Issue 5950, 285-289
<https://doi.org/10.1126/science.1178746>

 * Antibody sequence GenBank entries: GU272043 - GU272052
