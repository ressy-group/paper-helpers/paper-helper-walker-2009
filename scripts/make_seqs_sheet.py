#!/usr/bin/env python

"""
Make CSV of antibody seq attributes from downloaded GenBank files.
"""

import re
import sys
from csv import DictReader, DictWriter
from Bio import SeqIO

def parse_seq_desc(txt):
    match = re.match(r"([A-Z0-9.]+) Homo sapiens isolate ([A-Za-z0-9_]+) anti-HIV immunoglobulin (heavy|light) chain variable region mRNA, partial cds", txt)
    grps = match.groups()
    keys = ["Accession", "Antibody", "Chain"]
    fields = {k: v for k, v in zip(keys, grps)}
    fields["SeqID"] = fields["Antibody"] + "-" + {"heavy": "H", "light": "L"}[fields["Chain"]]
    return fields

FIELDS = [
    "SeqID",
    "Antibody",
    "Chain",
    "Accession",
    "Seq"]

def make_seqs_sheet(fastas):
    all_attrs = []
    for fasta in fastas:
        with open(fasta) as f_in:
            for record in SeqIO.parse(f_in, "fasta"):
                fields = parse_seq_desc(record.description)
                fields["Seq"] = str(record.seq)
                all_attrs.append(fields)
    writer = DictWriter(sys.stdout, FIELDS, lineterminator="\n")
    writer.writeheader()
    writer.writerows(all_attrs)

if __name__ == "__main__":
    make_seqs_sheet(sys.argv[1:])
